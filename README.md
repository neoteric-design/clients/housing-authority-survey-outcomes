# housing-authority-survey-outcomes

## Intro

This widget collects a list of qualifications from a user, and directs them to eligible programs based off their answers. The code is based on [Survey.JS](https://surveyjs.io/), written in standard HTML and CSS, and combined with [jQuery](https://jquery.com/).


## How To Use
This widget is designed for easy implementation in any site. It can be inserted within existing HTML, or can exist on a page of its own. All data is formatted within JS objects, and inserted into JS templates. As a default, the widget takes over an entire page. To insert within an existing page instead, copy the code below, and paste into your HTML.
```
<div id="widget">
  <div id="surveyNavigationTop"></div>
    <div id="surveyElement"></div>

    <div id="surveyResult"></div>
</div>
```


## Content

### Landing Page
All HTML content can be edited, however, it is very **important to note: ID names should not be changed.** Both Survey.JS and custom JS look for these specific ID names to render functions. Therefore, ID names should not be touched. Feel free to add your custom HTML elements, however.

### Question, helper title, and helper description
Question, helper title, and helper description are contained within the helperCopy object, with the keys corresponding to the survey page names. The key names (`people`, `income`, `demo`, etc...) are best left unchanged for proper functioning. Below is an example of how content should be formatted within the helperCopy object.

```
  const helperCopy = {
    "people":
      {
        question: "How many people are in your household?",
        title: "You and your family",
        description: "At least 1 member of your household must be 18 years or older to apply for services."
      },
    "income":
      {
        question: "What is your estimated annual household income?",
        title: "Your income counts",
        description: "To qualify for housing assistance, your family’s income must be below a certain amount. Your annual household income includes all yearly salaries, bonuses, commissions, overtime, tips and other income sources from all family members over 18 — before any deductions. Income limits differ based on the number of people in your household. We’ll help with the details when you apply."
      }
    }
  ```

### Income Levels
The Income level object contains keys ranging from 1-8, and their pairing values are a set of 4 income levels. The keys correspond to the number of people within a household-- so key '2' equates to 2 persons within the household. Key numbers and their paired income levels can be changed, but if so, it is important to update the `qualifiesFor` array within each income level. Here is the definition for the `qualifiesFor` terminology:
* `public` = "Public Housing"
* `affordable` = "Affordable Housing"
* `hcv` = "Housing Choice Voucher"
* `senior` = "Senior Housing"
* `accessible` = "Accessible Housing"
* `supportive` = "Supportive Housing"

The Object key, income levels, and qualifications array are all contained within the `incomeLevels` object, and should be formatted like below:

Qualifications Terms:

```
const incomeLevels = {
  ...

  '2': [
    { income: "$50 to $18,400", qualifiesFor: ["public", "affordable", "hcv"] },
    { income:   "$18,401 to $30,650", qualifiesFor: ["affordable", "hcv"] },
    { income:    "$30,651 to $49,050", qualifiesFor: ["hcv"] },
    { income:   "$49,051+", qualifiesFor: [] }
  ],
}
```

### Results Link list
The results page renders the qualified programs from the `allPrograms` object. This object includes a qualifications term (defined above) as a key, and a paired object containing a custom program label, program description, and button url. Programs can be added, removed, or edited. If adding a program, make sure to register it within one of the `qualifiesFor` arrays described above. 

```
const allPrograms = {
  "public":  {
    label: "Public Housing",
    providers: [
      {
        description: "Homes provided in one of HUD’s housing communities.",
        url: "http://www.google.com"
      }
    ]
  },

  "affordable":  {
    label: "Affordable Housing",
    providers: [
      {
        description: "Homes provided in one of our housing communities.",
        url: "http://www.google.com"
      }
    ]
  },

  "hcv":  {
    label: "Housing Choice Voucher",
    providers: [
      {
        description: "Households are provided with a rent subsidy that helps you afford rent at the residence of your choice.",
        url: "http://www.google.com"
      }
    ]
  },
}
```


## Styles

Only standard CSS is used for this application. Therefore, all styles are customizable. For some convenience, the primary color, along with primary font family are abstracted in CSS variables and placed at the top of the file. Feel free to add additional CSS for personalization.

### Deactivate Fade-In
To remove the Fade-in effect, comment out the `document.ready function()` within the JS file.
```
$(document).ready(() => {
  $("main").animate({ opacity: 1 }, 700)

  $(".sv-btn.sv-footer__next-btn, .sv-btn.sv-footer__complete-btn").each(function(index) {
    let el = $(this);
    $(el).on("click", function(){
      $("main").css("opacity", 0);
      $("main").animate({ opacity: 1 }, 700)
    })
  })

})
```
Then, in the CSS file, under the `main` selector, remove the `opacity` property. Fade-In should now be deactivated.


## Further Development
For questions or inquiries for further improvement, please contact the development team, [Neoteric Design](https://www.neotericdesign.com/)

### Testing

WIP. Open `test/test.html` in a browser to execute the tests and see the results.