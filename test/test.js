'use strict';
var expect = chai.expect

describe('evaluatePreferential', () => {
  context('under under_seventeen false, over_sixty_two true', () => {
    let results = { "under_seventeen": false, "over_sixty_two": true }
    it("qualifies for senior", () => {
      expect(evaluatePreferential(results)).to.include(allPrograms.senior)
    })
  })

  context('under under_seventeen true, over_sixty_two true', () => {
    let results = { "under_seventeen": true, "over_sixty_two": true }
    it("not qualifies for senior", () => {
      expect(evaluatePreferential(results)).to.not.include(allPrograms.senior)
    })
  })


  context('circumstance includes homeless', () => {
    let results = { "circumstance": ['homeless'] }

    it("qualifies for  supportive", () => {
      expect(evaluatePreferential(results)).to.include(allPrograms.supportive)
    })
  })

  context('circumstance includes veteran_homeless', () => {
    let results = { "circumstance": ['veteran_homeless'] }

    it("qualifies for  supportive", () => {
      expect(evaluatePreferential(results)).to.include(allPrograms.supportive)
    })
  })

  context('circumstance includes victim of domestic violence', () => {
    let results = { "circumstance": ['vdv'] }

    it("qualifies for preferential voucher with referral", () => {
      expect(evaluatePreferential(results)).to.include(allPrograms.pvwr)
      expect(evaluatePreferential(results)).to.not.include(allPrograms.supportive)
    })
  })
})

describe('evaluateIncome', () => {
  Object.keys(incomeLevels).forEach(numMinors => {
    incomeLevels[numMinors].forEach(level => {
      let expected = level['qualifiesFor'].map(programName => allPrograms[programName])

      it(`For ${numMinors} under seventeen and income: ${level.income} qualifies for ${expected.map(program => program.label)}`, () => {
        let actual = evaluateIncome({minors: numMinors, income: level.income})
        expect(actual).to.have.deep.members(expected)
      })
    })
  })
})

